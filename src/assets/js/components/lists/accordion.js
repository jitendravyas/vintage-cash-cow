'use strict';

$(function () {
  $('.list.list-accordion .list_item').on('click', function (e) {
    e.preventDefault();

    const item = this;

    if ($(item).hasClass('isOpen')) {
      $(item).removeClass('isOpen');
    } else {
      $(item).addClass('isOpen');
    }

    $(item).find('.item_text').slideToggle();
  });
});
